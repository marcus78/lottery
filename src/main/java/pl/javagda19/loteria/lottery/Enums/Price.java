package pl.javagda19.loteria.lottery.Enums;

public enum Price {

    SIX("4"),
    SEVEN("28"),
    EIGHT("112"),
    NINE("336"),
    TEN("840"),
    ELEVEN("1848"),
    TVELVE("3696");

    private String displayName;

    Price(String displayName){
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

//    Price eight = Price.EIGHT;
//    Price six = Price.SEVEN;
//
//    double szesc = Double.parseDouble(String.valueOf(six));
//    double osiem = Double.parseDouble(String.valueOf(eight));
//
//        System.out.println(szesc);
//        System.out.println(osiem);
//        System.out.println(szesc + osiem);
}
