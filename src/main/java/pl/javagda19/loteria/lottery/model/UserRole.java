//package pl.javagda19.loteria.lottery.model;
//
//import lombok.*;
//import org.springframework.data.annotation.Id;
//import org.springframework.format.annotation.DateTimeFormat;
//import org.springframework.scheduling.config.Task;
//
//import javax.persistence.*;
//import java.time.LocalDate;
//import java.util.List;
//import java.util.Set;
//
//@Entity
//@ToString
//@Getter
//@Setter
//@NoArgsConstructor
//@AllArgsConstructor
//public class UserRole {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
//
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    private LocalDate lastRenovation;
//
//    private String email;
//    private String password;
//    private String nickname;
//    private int rating;
//    private int game_cost;
//    private double game_win;
//
//    @ManyToMany
//    private Set<UserRole> roles;
//
//    @OneToMany(mappedBy = "user")
//    private List<Task> tasks;
//
//    @Override
//    public String toString() {
//        return email;
//    }
//
//}
