package pl.javagda19.loteria.lottery.model;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.Set;

public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate lastRenovation;

    private String email;
    private String password;
    private String nickname;
    private int rating;
    private int game_cost;
    private double game_win;

    @ManyToMany

    private Set<AppUserRole> roles;

    @OneToMany(mappedBy = "appUser")
    private Set<Los> draw;
}
