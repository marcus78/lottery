package pl.javagda19.loteria.lottery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoteryApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoteryApplication.class, args);
    }

}
